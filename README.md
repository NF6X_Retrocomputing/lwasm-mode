# lwasm-mode: An Emacs major mode for 6809 assembler source in lwasm format.

## Description

lwasm is a cross-assembler for 6809 and related processors. It is part of the lwtools package. The input format supported by lwasm is not compatible with the standard asm-mode in Emacs. In particular, comments and strings are handled quite differently.

lwasm-mode provides syntax highlighting for lwasm source code files. It does not provide automatic indentation, automatic completion, or any other convenience functions.

## Limitations

* Macro related highlighing is not yet implemented.
* To do: troll through instab.c in lwasm for more keywords to highlight.
* All testing so far has been done with hdbdos.asm, part of the toolshed package. So, anything not present in that file is untested, even if implemented.

## Related Projects

lwtools can be found at http://lwtools.projects.l-w.ca

Toolshed can be found at https://sourceforge.net/projects/toolshed/

## Copyright and License

Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>

lwasm-mode is released under GPLv3. See [COPYING.md](COPYING.md) for license terms.
