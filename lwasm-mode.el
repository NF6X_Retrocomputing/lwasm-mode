;;; lwasm-mode.el --- Major mode for 6809 assembler source in lwasm format.

;;; Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>

;; Author:   Mark J. Blair <nf6x@nf6x.net>
;; Version:  0.1
;; Keywords: lwasm

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; lwasm is a cross-assembler for 6809 and related processors. It is
;; part of the lwtools package. The input format supported by lwasm
;; is not compatible with the standard asm-mode in Emacs. In particular,
;; comments and strings are handled quite differently.
;;
;; lwasm-mode.el provides syntax highlighting for lwasm source code
;; files. It does not provide automatic indentation, automatic completion,
;; or any other convenience functions.
;;

;;; Limitations:
;;
;; Macro related highlighing is not yet implemented.
;;
;; To do: troll through instab.c in lwasm for more keywords to highlight.
;;
;; All testing so far has been done with hdbdos.asm, part of the
;; toolshed package. So, anything not present in that file
;; is untested, even if implemented.

;;; Code:

(defvar lwasm-mode-hook nil)

(defvar lwasm-mode-map
  (let ((map (make-sparse-keymap)))
  map)
  "Keymap for lwasm major mode.")


;;; Regular expressions used within `lwasm-font-lock-keywords'

(defconst lwasm-comment-line-regexp
  "^[;\\*].*"
  "Matches full line comment starting at first column.")

(defconst lwasm-conditional-with-operand-regexp
  (regexp-opt '("ifeq" "ifne" "if" "ifgt" "ifge" "iflt" "ifle"
                "ifdef" "ifpragma" "ifndef"))
  "Matches conditional assembly directives which require an operand.")

(defconst lwasm-conditional-without-operand-regexp
  (regexp-opt '("else" "endc"))
  "Matches conditional assembly directives which require no operand.")

(defconst lwasm-directive-with-operand-regexp
  (regexp-opt '("fcb" ".db" ".byte"
                "fdb" ".dw" ".word"
                "fqb" ".quad" ".4byte"
                "zmb" "zmd" "zmq"
                "rmb" ".blkb" ".ds" ".rs"
                "rmd" "rmq"
                "fill" "org" "setdp" "align" "os9" "mod"
                "end"
                ))
  "Matches misc. directives which require an operand.")

(defconst lwasm-directive-without-operand-regexp
  (regexp-opt '("reorg"
                "emod"))
  "Matches misc. directives which require no operand.")

(defconst lwasm-directive-with-filename-operand-regexp
  (regexp-opt '("include" "includebin" "use"))
  "Matches directives which take an unquoted filename operand.")

(defconst lwasm-directive-with-string-operand-regexp
  (regexp-opt '("fcc" ".ascii" ".str"
                "fcn" ".asciz" ".strz"
                "fcs" ".ascis" ".strs"
                ".module"))
  "Matches directives which take a string operand.")

(defconst lwasm-opcode-with-operand-regexp
  (regexp-opt '(
                ;; 6809 convenience instructions
                "asrd" "clrd" "comd" "lsld" "lsrd" "negd" "tstd"

                ;; 6309 convenience instructions
                "asrq" "clrq" "comq" "lsle" "lslf" "lslq" "lsrq"
                "nege" "negf" "negw" "negq" "tstq" "adca" "adcb"
                "adcd" "adcr" "adda" "addb" "addd" "adde" "addf"
                "addr" "addw" "aim" "anda" "andb" "andcc" "andd"
                "andr" "asl" "asr" "band" "bcc" "bcs" "beor" "beq"
                "bge" "bgt" "bhi" "bhs" "biand" "bieor" "bior" "bita"
                "bitb" "bitd" "bitmd" "ble" "blo" "bls" "blt" "bmi"
                "bne" "bor" "bpl" "bra" "brn" "bsr" "bvc" "bvs" "clr"
                "cmpa" "cmpb" "cmpd" "cmpe" "cmpf" "cmpr" "cmps"
                "cmpu" "cmpw" "cmpx" "cmpy" "com" "cwai" "dec" "divd"
                "divq" "eim" "eora" "eorb" "eord" "eorr" "exg" "inc"
                "jmp" "jsr" "lbcc" "lbcs" "lbeq" "lbge" "lbgt" "lbhi"
                "lbhs" "lble" "lblo" "lbls" "lblt" "lbmi" "lbne"
                "lbpl" "lbra" "lbrn" "lbsr" "lbvc" "lbvs" "lda" "ldb"
                "ldbt" "ldd" "lde" "ldf" "ldq" "lds" "ldu" "ldw" "ldx"
                "ldy" "ldmd" "leas" "leau" "leax" "leay" "lsl" "lsr"
                "muld" "neg" "oim" "ora" "orb" "orcc" "ord" "orr"
                "pshs" "pshu" "puls" "pulu" "rol" "ror" "sbca" "sbcb"
                "sbcd" "sbcr" "sta" "stb" "stbt" "std" "ste" "stf"
                "stq" "sts" "stu" "stw" "stx" "sty" "suba" "subb"
                "subd" "sube" "subf" "subr" "subw" "tfm"

                ;; compatibility opcodes for tfm in other assemblers
                "copy" "copy+" "tfrp" "copy-" "tfrm" "imp" "implode"
                "tfrs" "exp" "expand" "tfrr" "tfr" "tim" "tst"

                ;; for 6800 compatibility
                "cpx"))
  "Matches opcodes which require an operand.")

(defconst lwasm-opcode-without-operand-regexp
  (regexp-opt '(
                ;; 6309 convenience instructions
                "abx" "asla" "aslb" "asld" "asra" "asrb" "asrd" "clra"
                "clrb" "clrd" "clre" "clrf" "clrw" "coma" "comb"
                "comd" "come" "comf" "comw" "daa" "deca" "decb" "decd"
                "dece" "decf" "decw" "hcf" "inca" "incb" "incd" "ince"
                "incf" "incw" "lsla" "lslb" "lsld" "lsra" "lsrb"
                "lsrd" "lsrw" "mul" "nega" "negb" "negd" "nop" "pshsw"
                "pshuw" "pulsw" "puluw" "reset" "rhf" "rola" "rolb"
                "rold" "rolw" "rora" "rorb" "rord" "rorw" "rti" "rts"
                "sex" "sexw" "swi" "swi2" "swi3" "sync"
        
                ;; compatibility opcodes for tfm in other assemblers
                "tsta" "tstb" "tstd" "tste" "tstf" "tstw"

                ;; for 6800 compatibility
                "aba" "cba" "clc" "clf" "cli" "clif" "clv" "des" "dex"
                "dey" "ins" "inx" "iny" "sba" "sec" "sef" "sei" "seif"
                "sev" "tab" "tap" "tba" "tpa" "tsx" "txs" "wai"))
  "Matches opcodes which require no operand.")

(defconst lwasm-string-regexp
  "\\(\\([^ \t\n]\\).*?\\2\\)"
  "Matches string operands delimited by an arbitrary character")

(defconst lwasm-symbol-directive-regexp
  (regexp-opt '("equ" "=" "set"))
  "Matches directives which require a symbol and operand.")

(defconst lwasm-symbol-regexp
  "[a-zA-Z_\\.][a-zA-Z_\\.0-9\\$\\?@]*"
  "Matches valid symbol, not including beginning of line anchor.")

(defconst lwasm-warning-regexp
  (regexp-opt '("error" "warning"))
  "Matches error and warning directives with string operands.")


;;; keyword highlighting definitions

(defvar lwasm-font-lock-keywords
  (list
   ;; full-line comments with * or ; in first column
   `(,lwasm-comment-line-regexp . font-lock-comment-face)

   ;; Conditional assembly directives
   `(,(concat "^[ \t]+"
              "\\(" lwasm-conditional-with-operand-regexp "\\)" ; conditional directive
              "[ \t]+"                  ; whitespace
              "[^ \t\n]+"               ; operand
              "[ \t]*"                  ; whitespace
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-preprocessor-face)
     (2 font-lock-comment-face nil t))
   `(,(concat "^[ \t]+"
              "\\(" lwasm-conditional-without-operand-regexp "\\)" ; conditional directive
              "[ \t]*"                  ; whitespace
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-preprocessor-face)
     (2 font-lock-comment-face nil t))

   ;; Directives requiring a symbol and operand
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)" ; required symbol
              "[ \t]+"                  ; whitespace
              "\\(" lwasm-symbol-directive-regexp "\\)" ; directive
              "[ \t]+"                  ; whitespace
              "[^ \t\n]+"               ; operand
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-function-name-face)
     (2 font-lock-builtin-face)
     (3 font-lock-comment-face nil t))

   ;; Misc. directives
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                  ; whitespace
              "\\(" lwasm-directive-with-operand-regexp "\\)" ; directive
              "[ \t]+"                  ; whitespace
              "[^ \t\n]+"               ; operand
              "[ \t]*"                  ; whitespace
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-builtin-face)
     (3 font-lock-comment-face nil t))
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                  ; whitespace
              "\\(" lwasm-directive-without-operand-regexp "\\)" ; directive
              "[ \t]*"                  ; whitespace
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-builtin-face)
     (3 font-lock-comment-face nil t))

   ;; Directives with unquoted filename operands
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                  ; whitespace
              "\\(" lwasm-directive-with-filename-operand-regexp "\\)" ; directive
              "[ \t]+"                  ; whitespace
              "\\([^ \t\n]+\\)"         ; operand
              "[ \t]*"                  ; whitespace
              "\\([^\n]*\\)?"           ; optional comment
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-builtin-face)
     (3 font-lock-string-face)
     (4 font-lock-comment-face nil t))

   ;; Data directives with string operands
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                          ; whitespace
              "\\(" lwasm-directive-with-string-operand-regexp "\\)" ; directive
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-builtin-face)
     (,(concat lwasm-string-regexp "\\(.*\\)?")
      nil nil
      (1 font-lock-string-face)
      (3 font-lock-comment-face nil t))
     )

   ;; Error and warning directives with string operands.
   `(,(concat "^[ \t]+"                        ; whitespace
              "\\(" lwasm-warning-regexp "\\)" ; directive
              )
     (1 font-lock-warning-face)
     (,(concat lwasm-string-regexp "\\(.*\\)?")
      nil nil
      (1 font-lock-string-face)
      (3 font-lock-comment-face nil t))
     )

   ;; opcodes
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                        ; whitespace
              "\\(" lwasm-opcode-with-operand-regexp "\\)" ; opcode
              "[ \t]+"                        ; whitespace
              "[^ \t\n]+"                     ; operand
              "[ \t]*"                        ; whitespace
              "\\([^\n]*\\)?"                 ; optional comment
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-keyword-face)
     (3 font-lock-comment-face nil t))
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)?" ; optional symbol
              "[ \t]+"                        ; whitespace
              "\\(" lwasm-opcode-without-operand-regexp "\\)" ; opcode
              "[ \t]*"                        ; whitespace
              "\\([^\n]*\\)?"                 ; optional comment
              )
     (1 font-lock-function-name-face nil t)
     (2 font-lock-keyword-face)
     (3 font-lock-comment-face nil t))

   ;; Lone symbol
   `(,(concat "^\\(" lwasm-symbol-regexp "\\)")
     (1 font-lock-function-name-face))

   )
  "Keyword highlighting for lwasm mode.")


;;;###autoload
(defun lwasm-mode ()
  "Major mode for editing 6809 assembler source code for lwasm assembler, part of lwtools."
  (interactive)
  (kill-all-local-variables)
  (use-local-map lwasm-mode-map)
  (set (make-local-variable 'font-lock-defaults)
       '(lwasm-font-lock-keywords       ; keywords
         t                              ; keyword-only
         t                              ; case-fold
         ;; syntax-alist
         ;; syntax-begin
         ;; ...
         ))
  (setq major-mode 'lwasm-mode)
  (setq mode-name "lwasm")
  (run-hooks 'lwasm-mode-hook))

(provide 'lwasm-mode)

;;; lwasm-mode.el ends here
